import { Component, OnInit } from '@angular/core'; //****

import { TodoService } from './services/todo.service';
import ToDo from './models/to-do';

@Component({ //****
  selector: 'app-root', //****
  templateUrl: './app.component.html', //****
  styleUrls: ['./app.component.css'] //****
}) //****
export class AppComponent implements OnInit { //****
  title = 'app'; //****

  constructor(
    //Private todoservice will be injected into the component by Angular Dependency Injector
    private todoService: TodoService
  ) { }

  //Declaring the new todo Object and initializing it
  public newTodo: ToDo = new ToDo();

  //An Empty list for the visible todo list
  todos: ToDo[] = [];
  editTodos: ToDo[] = [];

  ngOnInit(): void {
    //At component initialization the
    this.todoService.getToDos()
      .subscribe(res => {
        //assign the todolist property to the proper http response
        this.todos = res;
        console.log(res);
      })
  }

  createTodo() {
    this.todoService.createTodo(this.newTodo)
      .subscribe(res => {
        //this.todosList.push(res.data)
        //assign the todolist property to the proper http response
        this.todos = res;
        console.log(res);
        this.newTodo = new ToDo();
      })
  }

  deleteTodo(todo: ToDo) {
    this.todoService.deleteTodo(todo)
      .subscribe(res => {
        //this.todos.splice(this.todos.indexOf(todo), 1);
        //assign the todolist property to the proper http response
        this.todos = res;
        console.log(res);
      });
  }

  editTodo(todo: ToDo) {
    console.log(todo);
    if (this.todos.includes(todo)) {
      if (!this.editTodos.includes(todo)) {
        this.editTodos.push(todo);
      } else {
        this.editTodos.splice(this.editTodos.indexOf(todo), 1);
        this.todoService.editTodo(todo).subscribe(res => {
          this.todos = res;
          console.log('Update Succesful');
        }, err => {
          this.editTodo(todo);
          console.error('Update Unsuccesful');
        })
      }
    }
  }

  submitTodo(event, todo: ToDo){
    if(event.keyCode ==13){
      this.editTodo(todo);
    }
  }

} //****
