import ToDo from '../models/to-do';
import { Observable } from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

@Injectable()
export class TodoService {

  api_url = 'http://localhost:8080';
  todoUrl = `${this.api_url}/todo`;

  constructor(
    private http: HttpClient
  ) { }

  //Create todo, takes a ToDo Object
  createTodo(todo: ToDo): Observable<ToDo[]>{
    return this.http.post(`${this.todoUrl}`, todo)
      .map(res  => res as ToDo[] || []);
  }

  //Read todo, takes no arguments
  getToDos(): Observable<ToDo[]>{
    let getUrl = `${this.todoUrl}/list`;
    return this.http.get(getUrl)
      .map(res  => res as ToDo[] || []);
  }

  //Delete todo, takes a ToDo Object
  deleteTodo(todo: ToDo): Observable<ToDo[]> {
    //Delete the object by the id
    let deleteUrl = `${this.todoUrl}/${todo._id}`;
    return this.http.delete(deleteUrl)
      .map(res  => res as ToDo[] || []);
  }

  //Update todo, takes a ToDo Object as parameter
  editTodo(todo:ToDo){
    let editUrl = `${this.todoUrl}/${todo._id}`
    //returns the observable of http put request
    return this.http.put(editUrl, {text: todo.text})
      .map(res  => res as ToDo[] || []);
  }

  //Default Error handling method.
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
